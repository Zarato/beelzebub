import argparse
import cv2
import matplotlib.pyplot as plt
import numpy as np
import os

from bench.benchmark import run_benchmark
from bench.utils import get_processor_name
from beelzebub.cv.haar import FaceDetector, EyesDetector, NoseDetector, MouthDetector
from beelzebub.cv.dnn import DNNDetector

parser = argparse.ArgumentParser(description='Run a benchmark')
parser.add_argument('image', type=str, help='input image')
parser.add_argument('-n', '--number', type=int, default=0, help='how many times to execute the ' \
                                                                'function')
parser.add_argument('-r', '--repeat', type=int, default=5, help='how many times to repeat the ' \
                                                                'timer')
parser.add_argument('-p', '--plot', type=bool, default=True, help='show the bar chart')
parser.add_argument('-o', '--output', type=str, default='chart.jpg', help='output file for the '
                                                                          'bar chart')

args = parser.parse_args()

image = cv2.imread(args.image)
h, w = image.shape[:2]
number = args.number
repeat = args.repeat
output = args.output

# prepare the functions for the benchmarks
face = FaceDetector()
eyes = EyesDetector()
mouth = MouthDetector()
nose = NoseDetector()
dnn_caffe = DNNDetector('caffe')
dnn_caffe.load()
dnn_tf = DNNDetector('tf')
dnn_tf.load()

face_detect = lambda: face.detect(image)
eyes_detect = lambda: eyes.detect(image)
mouth_detect = lambda: mouth.detect(image)
nose_detect = lambda: nose.detect(image)
dnn_caffe_detect = lambda: dnn_caffe.detect(image)
dnn_tf_detect = lambda: dnn_tf.detect(image)

# run benchmarks
face_bench = run_benchmark('Face', face_detect, number, repeat)
eyes_bench = run_benchmark('Eyes', eyes_detect, number, repeat)
mouth_bench = run_benchmark('Mouth', mouth_detect, number, repeat)
nose_bench = run_benchmark('Nose', nose_detect, number, repeat)
dnn_caffe_bench = run_benchmark('DNN (Caffe)', dnn_caffe_detect, number, repeat)
dnn_tf_bench = run_benchmark('DNN (TF)', dnn_tf_detect, number, repeat)

# print
print("CPU:", get_processor_name())
print(repr(face_bench))
print(repr(eyes_bench))
print(repr(mouth_bench))
print(repr(nose_bench))
print(repr(dnn_caffe_bench))
print(repr(dnn_tf_bench))

## plot
width = 0.35
msec = 1e-3  # nsec to msec

fig, ax = plt.subplots()

# haar
result = [face_bench, eyes_bench, mouth_bench, nose_bench, dnn_caffe_bench, dnn_tf_bench]
ind = np.arange(len(result))
mean = [b.mean / msec for b in result]
stddev = [b.stddev / msec for b in result]
p1 = ax.bar(ind, mean, width, yerr=stddev, label='OpenCV', ecolor='red', capsize=10)

ax.set_ylabel('Time (ms)')
ax.set_title(f'Benchmarks for {os.path.basename(args.image)} ({h}x{w})')
ax.set_yticks(np.arange(0, np.max(mean) + 10, 10))
ax.set_xticks(ind)
ax.set_xticklabels([b.name for b in result])
ax.legend()


def auto_label(bar):
    fps = lambda x: int(round(1000 / x))  # assume x is in msec
    for rect in bar:
        height = rect.get_height()
        ax.annotate('{:.2f}\nFPS: {}'.format(height, fps(height)), xy=(rect.get_x() + rect.get_width() / 2,
                                                                       height),
                    xytext=(0, 3),
                    textcoords="offset points", ha="center", va="bottom")


auto_label(p1)
# fig.tight_layout()

plt.savefig(args.output)

if args.plot:
    plt.show()
