# Beelzebub
Beelzebub is a Python library for detecting faces, eyes, mouths and noses.

Note: the library is still being written and is only experimental, it may give bad results.
## Installation
TODO

## Usage
```python
from beelzebub.cv.haar import FaceDetector, EyesDetector, MouthDetector, NoseDetector
import beelzebub

face = FaceDetector()
eyes = EyesDetector()
mouth = MouthDetector()
nose = NoseDetector()

image = "obama.jpg" # you can also use cv2.imread("obama.jpg")

face_result = face.detect(image)
# you can use parent to narrow down the search field
eyes_result = eyes.detect(image, parent=face_result[0])
mouth_result = mouth.detect(image, parent=face_result[0])
nose_result = nose.detect(image, parent=face_result[0])

colors = ['blue', 'red', 'magenta', 'yellow']

beelzebub.draw_save(image, "output.jpg", face_result, eyes_result, mouth_result, nose_result, colors=colors)
# beelzebub.plot(image, face_result, eyes_result, mouth_result, nose_result, colors=colors)
```

Output example:

![Obama](assets/images/obama_output.jpg)

## Examples
### Using OpenCV - Cascade
![Cascade](assets/images/obama_output.jpg)

### Using OpenCV - DNN
![DNN](assets/images/caffe.jpg)

## TODO

- [x] Haar Cascade in OpenCV
- [x] DNN Face Detector in OpenCV
- [ ] HoG Face Detector in Dlib
- [ ] CNN Face Detector in Dlib
- [ ] Anime Face Detector
- [ ] Hair detection
- [ ] Hair segmentation
- [ ] Landmarks

Further reading: [Face Detection](https://learnopencv.com/face-detection-opencv-dlib-and-deep-learning-c-python/)

## Benchmark

I made a benchmark in order to get an idea of the library's performance.
You can find the code that made it possible to make this benchmark in the `bench/` folder.

![Benchmark](assets/images/chart.jpg)

Output:
```
CPU:  Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz
--------------------------------
Face
        Mean: 20.2 msec
        Stddev: 832 usec
        Best: 19.6 msec
        Worst: 21.8 msec
--------------------------------
Eyes
        Mean: 33 msec
        Stddev: 257 usec
        Best: 32.5 msec
        Worst: 33.2 msec
--------------------------------
Mouth
        Mean: 8.14 msec
        Stddev: 1.66 msec
        Best: 6.46 msec
        Worst: 10.3 msec
--------------------------------
Nose
        Mean: 29.1 msec
        Stddev: 3.14 msec
        Best: 26.2 msec
        Worst: 35.2 msec
--------------------------------
DNN (Caffe)
        Mean: 25.2 msec
        Stddev: 3 msec
        Best: 22.2 msec
        Worst: 30.5 msec
--------------------------------
DNN (TF)
        Mean: 23.9 msec
        Stddev: 2.24 msec
        Best: 20.4 msec
        Worst: 26.2 msec
```

## License
[MIT](https://choosealicense.com/licenses/mit/)