from beelzebub.colors import Color, get_color


from typing import Union, List  # , Literal

import cv2
import numpy as np

from beelzebub.cv.dnn import DNNResult
from beelzebub.cv.haar.detector import HaarResult
from beelzebub.types import Object

Shape = str  # Literal['rect', 'ellipse', 'circle']
THICKNESS = 2


def draw(image: np.ndarray, shape: Shape, obj: Object, color: Color):
    shape = shape.lower()
    x, y, w, h = obj
    if shape == 'rect':
        cv2.rectangle(image, (x, y), (x + w, y + h), color, THICKNESS)
    elif shape == 'ellipse':
        center = (x + w // 2, y + h // 2)
        cv2.ellipse(image, center, (w // 2, h // 2), 0, 0, 360, color, THICKNESS)
    elif shape == 'circle':
        center = (x + w // 2, y + h // 2)
        radius = int(round((w + h) ** 0.25))
        cv2.circle(image, center, radius, color, THICKNESS)
    else:
        raise ValueError(f"{shape} is not a supported shape")


def plot(input_image: Union[str, np.ndarray], *args: Union[HaarResult, DNNResult],
         colors: Union[str, List[str], Color, List[Color]] = 'blue',
         shapes: Union[Shape, List[Shape]] = 'rect', title: str = 'Detection'):
    if isinstance(input_image, str):
        image = cv2.imread(input_image)
    else:
        image = np.array(input_image)  # make a copy

    n = len(args)

    # initialize shapes
    if isinstance(shapes, list):
        shapes = [shapes[i % len(shapes)] for i in range(n)]
    else:
        shapes = [shapes] * n

    # initialize colors
    if isinstance(colors, list):
        colors = [colors[i % len(colors)] for i in range(n)]
    else:
        colors = [colors] * n

    # draw on image
    for i in range(n):
        color = colors[i]
        color = get_color(color) if isinstance(color, str) else color

        shape = shapes[i]

        # draw
        for obj in args[i].objects:
            draw(image, shape, obj, color)

    # show image
    cv2.imshow(title, image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def draw_save(input_image: Union[str, np.ndarray], output: str, *args: Union[HaarResult, DNNResult],
              colors: Union[str, List[str], Color, List[Color]] = 'blue',
              shapes: Union[Shape, List[Shape]] = 'rect'):
    if isinstance(input_image, str):
        image = cv2.imread(input_image)
    else:
        image = np.array(input_image)  # make a copy

    n = len(args)

    # initialize shapes
    if isinstance(shapes, list):
        shapes = [shapes[i % len(shapes)] for i in range(n)]
    else:
        shapes = [shapes] * n

    # initialize colors
    if isinstance(colors, list):
        colors = [colors[i % len(colors)] for i in range(n)]
    else:
        colors = [colors] * n

    # draw on image
    for i in range(n):
        color = colors[i]
        color = get_color(color) if isinstance(color, str) else color

        shape = shapes[i]

        # draw
        for obj in args[i].objects:
            draw(image, shape, obj, color)

    # show image
    cv2.imwrite(output, image)
