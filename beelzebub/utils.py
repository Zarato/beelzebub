from pathlib import Path

import cv2
import numpy as np
from typing import Union

from beelzebub.types import Object


def crop(image: Union[str, np.ndarray], region: Object) -> np.ndarray:
    if isinstance(image, str):
        image = cv2.imread(image)

    x, y, w, h = region
    return image[y:y+h, x:x+w]


def get_data_file(name: str) -> str:
    return str(Path(__file__).resolve().parent / 'data' / name)
