from typing import Tuple, Dict

Color = Tuple[int, int, int]

BLUE: Color = (255, 0, 0)
GREEN: Color = (0, 128, 0)
RED: Color = (0, 0, 255)
CYAN: Color = (191, 191, 0)
MAGENTA: Color = (191, 0, 191)
YELLOW: Color = (0, 191, 191)
BLACK: Color = (0, 0, 0)
WHITE: Color = (255, 255, 255)

COLORS: Dict[str, Color] = {
    'b': BLUE,
    'g': GREEN,
    'r': RED,
    'c': CYAN,
    'm': MAGENTA,
    'y': YELLOW,
    'k': BLACK,
    'w': WHITE,
    'blue': BLUE,
    'green': GREEN,
    'red': RED,
    'cyan': CYAN,
    'magenta': MAGENTA,
    'yellow': YELLOW,
    'black': BLACK,
    'white': WHITE
}


def get_color(name: str) -> Color:
    keys = COLORS.keys()
    if name not in keys:
        raise ValueError(f"{name} is not a color")

    return COLORS[name]
