from dataclasses import dataclass
from typing import Tuple, Iterator, List

Size = Tuple[int, int]


@dataclass
class Object:
    x: int
    y: int
    w: int
    h: int

    def __iter__(self) -> Iterator:
        return iter((self.x, self.y, self.w, self.h))
