from typing import Union

import numpy as np

from beelzebub.cv.haar.detector import HaarDetector, HaarResult

from beelzebub.types import Size, Object
from beelzebub.utils import get_data_file

MOUTH_CASCADE = get_data_file('haarcascade_mouth.xml')


class MouthDetector(HaarDetector):

    def __init__(self, cascade: str = MOUTH_CASCADE):
        super(MouthDetector, self).__init__(cascade)

    def detect(self, input_image: Union[str, np.ndarray], scale_factor: float = 1.7, min_neighbors: int = 11,
               flags: int = None, min_size: Size = None, max_size: Size = None, num_detections: bool = False,
               reject_levels: bool = False, parent: Object = None) -> HaarResult:
        return super(MouthDetector, self).detect(input_image, scale_factor, min_neighbors,
                                                 flags, min_size, max_size, num_detections, reject_levels, parent)
