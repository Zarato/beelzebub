from beelzebub.cv.haar.eyes import EyesDetector
from beelzebub.cv.haar.face import FaceDetector
from beelzebub.cv.haar.mouth import MouthDetector
from beelzebub.cv.haar.nose import NoseDetector
