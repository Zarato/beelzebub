from dataclasses import dataclass

import cv2
from typing import Union, List, Iterator
import numpy as np
import os

from beelzebub.types import Size, Object


@dataclass
class HaarResult:
    objects: List[Object]
    neighbours: List[int] = None
    weights: np.ndarray = None

    def __iter__(self) -> Iterator:
        return iter(self.objects)

    def __len__(self) -> int:
        return len(self.objects)

    def __getitem__(self, item) -> Object:
        return self.objects[item]


class HaarDetector:
    cascade_file: str
    model: cv2.CascadeClassifier

    def __init__(self, cascade_file: str):
        if not os.path.isfile(cascade_file):
            raise ValueError(f"{cascade_file} is not a file !")

        self.cascade_file = cascade_file
        self.model = cv2.CascadeClassifier(cascade_file)

    def detect(self, input_image: Union[str, np.ndarray], scale_factor: float = 1.1, min_neighbors: int = 3,
               flags: int = None, min_size: Size = None, max_size: Size = None, num_detections: bool = False,
               reject_levels: bool = False, parent: Object = None) \
            -> HaarResult:
        if isinstance(input_image, str):
            image = cv2.imread(input_image)
        else:
            image = input_image

        # crop the image in order to analyse only the desired region
        if parent:
            x, y, w, h = parent
            image = image[y:y + h, x:x + w]

        neighbours = None
        weights = None

        # convert to gray
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = cv2.equalizeHist(image)

        if reject_levels:
            objects, neighbours, weights = self.model.detectMultiScale3(image, scaleFactor=scale_factor,
                                                                        minNeighbors=min_neighbors, flags=flags,
                                                                        minSize=min_size, maxSize=max_size,
                                                                        outputRejectLevels=reject_levels)
        elif num_detections:
            objects, neighbours = self.model.detectMultiScale2(image, scaleFactor=scale_factor,
                                                               minNeighbors=min_neighbors, flags=flags,
                                                               minSize=min_size, maxSize=max_size)
        else:
            objects = self.model.detectMultiScale(image, scaleFactor=scale_factor,
                                                  minNeighbors=min_neighbors, flags=flags,
                                                  minSize=min_size, maxSize=max_size)

        if parent:
            x, y, _, _ = parent
            for obj in objects:
                obj[0] += x
                obj[1] += y

        # convert to Object class
        objects = [Object(*rect) for rect in objects]

        return HaarResult(objects, neighbours, weights)
