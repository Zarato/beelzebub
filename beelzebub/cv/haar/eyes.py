from typing import Union

import numpy as np

from beelzebub.cv.haar.detector import HaarDetector, HaarResult
from beelzebub.types import Size, Object
from beelzebub.utils import get_data_file

EYES_CASCADE = get_data_file('haarcascade_eye_tree_eyeglasses.xml')


class EyesDetector(HaarDetector):

    def __init__(self, cascade: str = EYES_CASCADE):
        super(EyesDetector, self).__init__(cascade)

    def detect(self, input_image: Union[str, np.ndarray], scale_factor: float = 1.1, min_neighbors: int = 1,
               flags: int = None, min_size: Size = None, max_size: Size = None, num_detections: bool = False,
               reject_levels: bool = False, parent: Object = None) -> HaarResult:
        return super(EyesDetector, self).detect(input_image, scale_factor, min_neighbors,
                                                flags, min_size, max_size, num_detections, reject_levels, parent)

