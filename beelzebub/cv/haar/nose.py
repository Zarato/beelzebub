from typing import Union

import numpy as np

from beelzebub.cv.haar.detector import HaarDetector, HaarResult

from beelzebub.types import Size, Object
from beelzebub.utils import get_data_file

NOSE_CASCADE = get_data_file('haarcascade_nose.xml')


class NoseDetector(HaarDetector):

    def __init__(self, cascade: str = NOSE_CASCADE):
        super(NoseDetector, self).__init__(cascade)

    def detect(self, input_image: Union[str, np.ndarray], scale_factor: float = 1.3, min_neighbors: int = 5,
               flags: int = None, min_size: Size = None, max_size: Size = None, num_detections: bool = False,
               reject_levels: bool = False, parent: Object = None) -> HaarResult:
        return super(NoseDetector, self).detect(input_image, scale_factor, min_neighbors,
                                                flags, min_size, max_size, num_detections, reject_levels, parent)
