from beelzebub.cv.dnn.model import MODELS, get_model, Model, TENSORFLOW, CAFFE
from beelzebub.cv.dnn.detector import DNNDetector, DNNResult
