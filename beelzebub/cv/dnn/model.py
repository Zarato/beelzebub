from dataclasses import dataclass
from typing import Dict

import cv2.dnn
import urllib.request
import os

from beelzebub.utils import get_data_file

FILES = {
    'deploy.prototxt': 'https://github.com/opencv/opencv/raw/master/samples/dnn/face_detector/deploy.prototxt',
    'res10_300x300_ssd_iter_140000_fp16.caffemodel':
        'https://raw.githubusercontent.com/opencv/opencv_3rdparty/dnn_samples_face_detector_20180205_fp16/res10_300x300_ssd_iter_140000_fp16.caffemodel',
    'opencv_face_detector.pbtxt': 'https://github.com/opencv/opencv/raw/master/samples/dnn/face_detector/opencv_face_detector.pbtxt',
    'opencv_face_detector_uint8.pb': 'https://raw.githubusercontent.com/opencv/opencv_3rdparty/dnn_samples_face_detector_20180220_uint8/opencv_face_detector_uint8.pb'
}


def download_file(path: str):
    name = os.path.basename(path)
    keys = FILES.keys()
    if name not in keys:
        raise ValueError(f"{name} cannot be downloaded because the name is invalid. Valid names: {keys}")

    with urllib.request.urlopen(FILES[name]) as response, open(path, 'wb') as file:
        data = response.read()
        file.write(data)


@dataclass
class Model:
    framework: str
    proto: str
    weights: str

    def load(self) -> cv2.dnn_Net:
        if not os.path.isfile(self.proto):
            self.proto = get_data_file(self.proto)
            if not os.path.isfile(self.proto):
                download_file(self.proto)

        if not os.path.isfile(self.weights):
            self.weights = get_data_file(self.weights)
            if not os.path.isfile(self.weights):
                download_file(self.weights)

        if self.framework.lower() == 'tf':
            return cv2.dnn.readNetFromTensorflow(self.weights, self.proto)
        elif self.framework.lower() == 'caffe':
            return cv2.dnn.readNetFromCaffe(self.proto, self.weights)

        raise ValueError(f"The framework {self.framework} is not supported yet.")


TENSORFLOW = Model('tf', 'opencv_face_detector.pbtxt', 'opencv_face_detector_uint8.pb')
CAFFE = Model('caffe', 'deploy.prototxt', 'res10_300x300_ssd_iter_140000_fp16.caffemodel')

PARAMS = {
    'tf': [(300, 300), (104, 177, 123)],  # (size, mean)
    'caffe': [(300, 300), (104, 177, 123)]
}

MODELS = {
    'tf': TENSORFLOW,
    'caffe': CAFFE
}


def get_model(framework: str) -> Model:
    framework = framework.lower()
    keys = MODELS.keys()
    if framework not in keys:
        raise ValueError(f"The framework {framework} is not supported yet.")

    return MODELS[framework]


def get_model_parameters(framework: str) -> Dict:
    framework = framework.lower()
    keys = PARAMS.keys()
    if framework not in keys:
        raise ValueError(f"The framework {framework} is not supported yet.")

    return {'size': PARAMS[framework][0], 'mean': PARAMS[framework][1]}
