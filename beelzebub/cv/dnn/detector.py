from dataclasses import dataclass
from typing import Union, Tuple, Iterator, List

import cv2
import numpy as np

from beelzebub.cv.dnn.model import Model, get_model, get_model_parameters
from beelzebub.types import Size, Object


@dataclass
class DNNResult:
    objects: List[Object]
    confidences: List[float]

    def __iter__(self) -> Iterator:
        return ((self.objects[i], self.confidences[i]) for i in range(len(self.objects)))

    def __len__(self) -> int:
        return len(self.objects)

    def __getitem__(self, item) -> Object:
        return self.objects[item]


class DNNDetector:
    model: Model
    net: cv2.dnn_Net
    size: Size
    mean: Tuple[int, int, int]

    def __init__(self, model: Union[str, Model] = 'caffe', size: Size = None, mean: Tuple[int, int, int] = None):
        if isinstance(model, str):
            params = get_model_parameters(model)
            model = get_model(model)
            size = params['size']
            mean = params['mean']
        else:
            if size is None or mean is None:
                raise ValueError("You must specify a resize and mean subtraction")

        self.model = model
        self.size = size
        self.mean = mean

    def load(self):
        self.net = self.model.load()

    @property
    def is_loaded(self) -> bool:
        return hasattr(self, 'net')

    def detect(self, image: Union[str, np.ndarray], scale_factor: float = 1.0, size: Size = None,
               mean: Tuple[int, int, int] =
               None, threshold: float = 0.8, swap_rb: bool = False, crop: bool = False,
               ddepth: int = cv2.CV_32F) -> DNNResult:
        if not self.is_loaded:
            self.load()

        if isinstance(image, str):
            image = cv2.imread(image)
        else:
            image = image

        if size is None:
            size = self.size

        if mean is None:
            mean = self.mean

        h, w = image.shape[:2]
        resized = cv2.resize(image, size)
        blob = cv2.dnn.blobFromImage(resized, scalefactor=scale_factor, size=size, mean=mean, swapRB=swap_rb,
                                     crop=crop, ddepth=ddepth)

        self.net.setInput(blob)
        faces: np.ndarray = self.net.forward()

        confidences = faces[0, 0, :, 2] > threshold
        boxes = faces[0, 0, confidences, 3:7] * np.array([w, h, w, h])
        boxes = boxes.astype('int')

        objects = [Object(b[0], b[1], b[2] - b[0], b[3] - b[1]) for b in boxes]  # convert to (x,y,w,h)

        return DNNResult(objects, faces[0, 0, confidences, 2])
