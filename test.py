import beelzebub

sample = "kanye.jpg"
output = "output.jpg"

face = beelzebub.FaceDetector()
eyes = beelzebub.EyesDetector()
mouth = beelzebub.MouthDetector()
nose = beelzebub.NoseDetector()

face_result = face.detect(sample, min_neighbors=1)
print("Number of faces:", len(face_result))

person_face = face_result[0]

# eyes_result = eyes.detect(sample, scale_factor=1.1, parent=person_face, min_neighbors=1)
eyes_result = eyes.detect(sample, parent=person_face)
# mouth_result = mouth.detect(sample, scale_factor=1.7, parent=person_face, min_neighbors=11)
mouth_result = mouth.detect(sample, parent=person_face)
# nose_result = nose.detect(sample, scale_factor=1.3, parent=person_face, min_neighbors=5)
nose_result = nose.detect(sample, parent=person_face)

print("Number of eyes:", len(eyes_result))
print("Number of mouths:", len(mouth_result))
print("Number of noses:", len(nose_result))

colors = ['blue', 'red', 'magenta', 'yellow']

beelzebub.draw_save(sample, output, face_result, eyes_result, mouth_result, nose_result, colors=colors)
# beelzebub.plot(sample, face_result, eyes_result, mouth_result, nose_result, colors=colors)
