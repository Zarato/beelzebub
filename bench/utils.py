import os
import platform
import subprocess
import re

units = {"nsec": 1e-9, "usec": 1e-6, "msec": 1e-3, "sec": 1.0}
precision = 3


def format_time(dt: float) -> str:
    scales = [(scale, unit) for unit, scale in units.items()]
    scales.sort(reverse=True)
    for scale, unit in scales:
        if dt >= scale:
            break

    return "%.*g %s" % (precision, dt / scale, unit)


def get_processor_name() -> str:
    if platform.system() == "Windows":
        return platform.processor()
    elif platform.system() == "Darwin":
        os.environ['PATH'] = os.environ['PATH'] + os.pathsep + '/usr/sbin'
        command = "sysctl -n machdep.cpu.brand_string"
        return  subprocess.check_output(command).strip().decode()
    elif platform.system() == "Linux":
        command = "cat /proc/cpuinfo"
        info = subprocess.check_output(command, shell=True).strip().decode()
        for line in info.split("\n"):
            if "model name" in line:
                return re.sub(r".*model name.*:", "", line, 1)

    raise Exception(f"{platform.system()} is not supported")
