from dataclasses import dataclass
from timeit import Timer

from bench.utils import format_time
from typing import Callable

import numpy as np


@dataclass
class BenchmarkResult:
    # nsec
    best: float
    worst: float
    mean: float
    stddev: float
    name: str

    def __str__(self) -> str:
        return "%s : %s ± %s" % (self.name, format_time(self.mean), format_time(self.stddev))

    def __repr__(self) -> str:
        result = '-' * max(len(self.name), 32) + "\n"
        result += self.name + "\n"
        result += "\tMean: " + format_time(self.mean) + "\n"
        result += "\tStddev: " + format_time(self.stddev) + "\n"
        result += "\tBest: " + format_time(self.best) + "\n"
        result += "\tWorst: " + format_time(self.worst)

        return result


def run_benchmark(name: str, func: Callable, number: int = 0, repeat: int = 5) -> BenchmarkResult:
    t = Timer(stmt=func)
    number, _ = t.autorange()
    raw_timings = t.repeat(repeat=repeat, number=number)

    timings = np.array(raw_timings) / number  # faster with numpy
    best = np.min(timings)
    worst = np.max(timings)
    mean = np.mean(timings)
    stddev = np.std(timings)

    return BenchmarkResult(best, worst, mean, stddev, name)
